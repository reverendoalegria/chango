name := "chango"

version := "0.1"

scalaVersion := "2.12.5"

libraryDependencies += "com.github.tototoshi" %% "scala-csv" % "1.3.5"

libraryDependencies += "com.github.spullara.mustache.java" % "compiler" % "0.9.5"

libraryDependencies += "io.gatling" %% "jsonpath" % "0.6.11"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.4"

libraryDependencies += "com.nrinaudo" %% "kantan.xpath" % "0.4.0"

libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.5.5"