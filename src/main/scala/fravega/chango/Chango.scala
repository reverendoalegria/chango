package fravega.chango


import java.io.{ByteArrayOutputStream, StringReader, StringWriter}
import java.net.URI
import java.nio.file.{Path, Paths}

import org.apache.http.client.utils.URIBuilder

import scala.util.Try

object ChangoOrdersFindFilcar extends App {

  val input = ChangoCsvFile(Paths.get(args(0)))

  val output = ChangoCsvFile(Paths.get(args(1)))

  implicit val http: HttpClient = new ACHttpClient()

  val orderReq = ChangoPatternsLibrary.vtexOmsOrderRequest()

  val stepOmsOrder = ChangoStep(orderReq, Seq(
    JsonPathChangoExtractor(
      PathExtraction("shipping-street", "$.shippingData.address.street"),
      PathExtraction("shipping-number", "$.shippingData.address.number"),
      PathExtraction("shipping-state", "$.shippingData.address.state"),
      PathExtraction("shipping-city", "$.shippingData.address.city")
    )
  ))

  val unigisReq = ChangoPatternsLibrary.unigisRequest()
  val stepUnigis = ChangoStep(unigisReq, Seq(
    XPathChangoExtractor(
      PathExtraction("unigis-filcar", "//GeoPoligono[tipo='Filcar']/nombre/text()")
    )
  ))

  val definition = ChangoDef(input, output, Seq(stepOmsOrder, stepUnigis))

  new Chango().process(definition)

}

object ChangoBookinsReworkFilcar extends App {

  val input = ChangoCsvFile(Paths.get(args(0)))

  val output = ChangoCsvFile(Paths.get(args(1)))

  implicit val http: HttpClient = new ACHttpClient()

  val ORDERID_PLACEHOLDER = "orderId"
  val STREET_PLACEHOLDER = "shipping-street"
  val NUMBER_PLACEHOLDER = "shipping-number"
  val STATE_PLACEHOLDER = "shipping-state"
  val CITY_PLACEHOLDER = "shipping-city"
  val FILCAR_PLACEHOLDER = "unigis-filcar"

  val integradorReq = ChangoPatternsLibrary.integradorFindOrderByReserveNumberRequest("Reserva")
  val stepIntegrador = ChangoStep(integradorReq, Seq(
    JsonPathChangoExtractor(
      PathExtraction(ORDERID_PLACEHOLDER, "$._embedded.persistOrders[:1].orderId")
    )
  ))

  val orderReq = ChangoPatternsLibrary.vtexOmsOrderRequest(ORDERID_PLACEHOLDER)
  val stepOmsOrder = ChangoStep(orderReq, Seq(
    JsonPathChangoExtractor(
      PathExtraction(STREET_PLACEHOLDER, "$.shippingData.address.street"),
      PathExtraction(NUMBER_PLACEHOLDER, "$.shippingData.address.number"),
      PathExtraction(STATE_PLACEHOLDER, "$.shippingData.address.state"),
      PathExtraction(CITY_PLACEHOLDER, "$.shippingData.address.city")
    )
  ))

  val unigisReq = ChangoPatternsLibrary.unigisRequest(STREET_PLACEHOLDER, NUMBER_PLACEHOLDER, CITY_PLACEHOLDER, STATE_PLACEHOLDER)
  val stepUnigis = ChangoStep(unigisReq, Seq(
    XPathChangoExtractor(
      PathExtraction(FILCAR_PLACEHOLDER, "//GeoPoligono[tipo='Filcar']/nombre/text()")
    )
  ))

  val definition = ChangoDef(input, output, Seq(stepIntegrador, stepOmsOrder, stepUnigis))

  new Chango().process(definition)

}

object ChangoPatternsLibrary {
  import ChangoReqMethods._

  def integradorFindOrderByReserveNumberRequest(reservePlaceholder: String = "reserveNumber")(implicit http: HttpClient) =
    ChangoReq(GET, s"http://integrador.fravega.com/persistOrders/search/findByReserveNumberOrderByTsAsc?size=1&reserveNumber={{$reservePlaceholder}}", None, Seq())

  val vtexAuthKey = "vtexappkey-fravega-EHZMRD"
  val vtexAuthToken = "NZEXXCVUOILJSRFZXIYWENFXKVHZQZPFVICBQISVLIYGBDBJBKUAPSDAUGNBVWDPTGNJOEJPPCLVPYWGTXXGEPPDFCGJUQAOEINVPRUWBFLNHHKVKGIXYWKDQKDMYZUY"
  val vtexHeaders = Seq("X-VTEX-API-AppToken" -> vtexAuthToken, "X-VTEX-API-AppKey" -> vtexAuthKey)
  def vtexOmsUrl(placeholder: String) = s"http://fravega.vtexcommercestable.com.br/api/oms/pvt/orders/{{$placeholder}}"
  def vtexOmsOrderRequest(placeholder: String = "VTEX order ID")(implicit http: HttpClient) = ChangoReq(method = ChangoReqMethods.GET, vtexOmsUrl(placeholder), None, vtexHeaders)

  val unigisHeaders = Seq("Accept" -> "application/xml,text/xml", "accept-encoding" -> "gzip, deflate, br")
  def unigisURLTemplate(streetPH: String, numberPH: String, cityPH: String, statePH: String) =
    s"https://cloud.unigis.com/FRAVEGA/MAPI/SOAP/geographic/SERVICE.ASMX/buscarDomicilioSimple?calle={{$streetPH}}&numero={{$numberPH}}&partido={{$cityPH}}&provincia={{$statePH}}&apiKey=&interseccion=&barrio=&localidad=&pais=Argentina"
  def unigisRequest(streetPH: String = "shipping-street", numberPH: String = "shipping-number", cityPH: String = "shipping-city", statePH: String = "shipping-state")(implicit http: HttpClient) =
    ChangoReq(ChangoReqMethods.GET, unigisURLTemplate(streetPH, numberPH, cityPH, statePH), None, Seq())
}

class Chango(implicit http: HttpClient) {

  def writeCsv(output: ChangoCsvFile, values: Seq[Map[String, String]]): Unit = {
    import com.github.tototoshi.csv.CSVWriter

    val writer = CSVWriter.open(output.file.toFile)
    val headers = values.flatMap(_.keySet).distinct
    writer.writeRow(headers)
    values.foreach { row =>
      writer.writeRow(row.values.toSeq)
    }
    writer.flush()

  }

  def process(definition: ChangoDef): Unit = {

    val contextVariables = iterateCsv(definition.input) { lineMap =>

      val ctx = ChangoContext(lineMap)

      val newCtx =
        Try {
          definition.steps.foldLeft(ctx) { (lastCtx, step) =>
            println(s"Executing step $step...")
            val newNewCtx = step.execute(lastCtx)
            newNewCtx
          }
        }.getOrElse(ctx)

      newCtx.variables
    }

    writeCsv(definition.output, contextVariables)

  }

  def iterateCsv(input: ChangoCsvFile)(fnLine: Map[String, String] => Map[String, String]): Seq[Map[String, String]] = {
    import com.github.tototoshi.csv._
    val reader = CSVReader.open(input.file.toFile)
    reader.toStreamWithHeaders.map(fnLine)
  }

}

case class ChangoContext(variables: Map[String, String]) {
  import java.util
  import com.github.mustachejava.DefaultMustacheFactory

  import scala.collection.JavaConverters._
  val mustacheFactory = new DefaultMustacheFactory()

  private lazy val javaMap: util.Map[String, String] = mapAsJavaMap(variables)

  def apply(template: String): String = {
    val writer = new StringWriter()
    val barba = mustacheFactory.compile(new StringReader(template), template + "_key")
    val newWriter = barba.execute(writer, javaMap)
    newWriter.toString
  }
}

sealed trait ChangoReqMethod
object ChangoReqMethods {
  case object GET extends ChangoReqMethod {
    override def toString: String = "GET"
  }
  case object POST extends ChangoReqMethod {
    override def toString: String = "POST"
  }
  case object PUT extends ChangoReqMethod {
    override def toString: String = "PUT"
  }
}

case class ChangoCsvFile(file: Path)
sealed trait ChangoExtractor {
  def extract(resp: ChangoResp, ctx: ChangoContext): ChangoContext
}

case class PathExtraction(name: String, path: String)
case class JsonPathChangoExtractor(paths: PathExtraction*) extends ChangoExtractor {
  import com.fasterxml.jackson.databind.ObjectMapper
  import io.gatling.jsonpath._
  import java.util

  private val mapper = new ObjectMapper()

  private val queries = paths.map(p => p -> JsonPath.compile(p.path).fold(err => throw new Exception(err.reason), jp => jp))

  def extract(resp: ChangoResp, ctx: ChangoContext): ChangoContext = {
    // println(s"Extracting json-path [$path] value from response: $resp")
    val valueMap: util.HashMap[_, _] = mapper.readValue(resp.body, classOf[java.util.HashMap[_, _]])

    queries.foldLeft(ctx) { case (accCtx, (extr, jp)) =>
      val iterator = jp.query(valueMap)
      val jsonValue = iterator.toSeq.headOption.map(_.toString).getOrElse("")
      println(s"Extracted json-path [$extr] value from response: $jsonValue")
      accCtx.copy(variables = accCtx.variables.updated(extr.name, jsonValue))
    }

  }
}

case class XPathChangoExtractor(paths: PathExtraction*) extends ChangoExtractor {
  import kantan.xpath._
  import kantan.xpath.implicits._
  import org.w3c.dom.Node

  private val xpathCompiler = XPathCompiler.builtIn

  private val xpaths = paths.flatMap(p => xpathCompiler.compile(p.path) match {
    case Left(_) => None
    case Right(value) => Some(p -> value)
  })

  override def extract(resp: ChangoResp, ctx: ChangoContext): ChangoContext = {
    xpaths.foldLeft(ctx) { case (accCtx, (extr, xpath)) =>
      val result = resp.body.evalXPath[List[Node]](xpath)
      val nodes = result.fold(err => throw new Exception(err.getMessage), nodes => nodes)
      val text = nodes.filter(_.getNodeType == Node.TEXT_NODE).map(_.getTextContent).mkString(",")
      println(s"Extracted xpath [$extr] value from response: $text")
      accCtx.copy(variables = accCtx.variables.updated(extr.name, text))
    }
  }
}

case class ChangoReq(method: ChangoReqMethod, urlTemplate: String, bodyTemplate: Option[String], headersTemplates: Seq[(String, String)])(implicit http: HttpClient) {
  import fravega.chango.ChangoReqMethods._

  def execute(ctx: ChangoContext): ChangoResp = {
    val url = buildURL(ctx)
    val body = buildBody(ctx)
    val headers = buildHeaders(ctx)
    val resp = method match {
      case GET => http.get(url, headers)
      case POST => http.post(url, headers, body)
      case PUT => http.put(url, headers, body)
    }
    ChangoResp(resp.body, resp.status, resp.headers)
  }

  def buildURL(ctx: ChangoContext): String = ctx.apply(urlTemplate)

  def buildBody(ctx: ChangoContext): Option[String] = bodyTemplate.map(ctx.apply)

  def buildHeaders(ctx: ChangoContext): Seq[(String, String)] = headersTemplates.map(t => t._1 -> ctx.apply(t._2))
}

case class ChangoResp(body: String, status: Int, headers: Seq[(String, String)])
case class ChangoStep(req: ChangoReq, extractors: Seq[ChangoExtractor]) {
  def execute(ctx: ChangoContext): ChangoContext = {
    val resp = req.execute(ctx)
    extractors.foldLeft(ctx)((newCtx, extractor) => extractor.extract(resp, newCtx))
  }
}

case class ChangoDef(input: ChangoCsvFile, output: ChangoCsvFile, steps: Seq[ChangoStep])

case class HttpResponse(status: Int, body: String, headers: Seq[(String, String)])

trait HttpClient {
  def get(url: String, headers: Seq[(String, String)]): HttpResponse
  def post(url: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse
  def put(url: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse
}

class JDKHttpClient extends HttpClient {
  import java.net.URL
  import java.net.HttpURLConnection
  import java.nio.charset.StandardCharsets
  import scala.io.Source

  override def get(url: String, headers: Seq[(String, String)]): HttpResponse = {
    doCall(url, "GET", headers, None)
  }

  override def post(url: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse = {
    doCall(url, "POST", headers, body)
  }

  override def put(url: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse = {
    doCall(url, "PUT", headers, body)
  }

  private def doCall(url: String, method: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse = {
    val httpUrl = new URL(url)
    val conn = httpUrl.openConnection().asInstanceOf[HttpURLConnection]
    conn.setRequestMethod(method)
    headers.foreach { case (key, value) => conn.addRequestProperty(key, value) }
    body.foreach { b =>
      conn.setDoOutput(true)
      conn.getOutputStream.write(b.getBytes(StandardCharsets.UTF_8))
    }
    val respStatus = conn.getResponseCode
    val respBody = Source.fromInputStream(conn.getInputStream).mkString
    // conn.getHeaderFields.
    HttpResponse(respStatus, respBody, Seq())
  }
}

class ACHttpClient extends HttpClient {
  import org.apache.http.client.methods.{HttpGet, HttpPost, HttpPut, HttpRequestBase}
  import org.apache.http.impl.client.{CloseableHttpClient, HttpClients}
  import java.net.URLDecoder

  private val http: CloseableHttpClient = HttpClients.createDefault()

  def parseUrlParameters(url: String): Map[String, String] = {
    url.split("&").filter(_.nonEmpty).map( v => {
      val m =  v.split("=", 2).map(s => URLDecoder.decode(s, "UTF-8"))
      m(0) -> m(1)
    }).toMap
  }

  def splitUrlParameters(url: String): (String, String) = {
    val parts = url.split("\\?", 2)
    if (parts.length < 2) url -> ""
    else parts(0) -> parts(1)
  }

  override def get(url: String, headers: Seq[(String, String)]): HttpResponse = {
    doCall(new HttpGet(), url, headers, None)
  }

  override def post(url: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse = {
    val params = parseUrlParameters(url)
    doCall(new HttpPost(), url, headers, body)
  }

  override def put(url: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse = {
    val params = parseUrlParameters(url)
    doCall(new HttpPut(), url, headers, body)
  }

  private def doCall(method: HttpRequestBase, url: String, headers: Seq[(String, String)], body: Option[String]): HttpResponse = {
    println(s"Calling HTTP [$method] $url...")
    val validURI: URI = rebuildURI(url)
    method.setURI(validURI)
    headers.foreach { case (k, v) => method.addHeader(k, v) }

    val response = http.execute(method)
    val responseBody = Option(response.getEntity).map { entity =>
      val bos = new ByteArrayOutputStream()
      entity.writeTo(bos)
      bos.toString(Option(entity.getContentEncoding).map(_.getValue).getOrElse("UTF-8"))
    }
    val responseHeaders = response.getAllHeaders.map { h => h.getName -> h.getValue }.toSeq
    val statusCode = response.getStatusLine.getStatusCode
    println(s"Called HTTP [$method] $url. Status: $statusCode")
    HttpResponse(statusCode, responseBody.getOrElse(""), responseHeaders)
  }

  private def rebuildURI(url: String) = {
    val (preURL, paramsURL) = splitUrlParameters(url)
    val uriBuilder = new URIBuilder(preURL)
    val params = parseUrlParameters(paramsURL)
    params.foreach { case (k, v) => uriBuilder.setParameter(k, v) }
    val validURL = uriBuilder.build()
    validURL
  }
}